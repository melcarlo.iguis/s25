db.course_booking.insertMany(
	[
		{ "courseId" : "C001" , "studentId" : "S004", "isCompleted" : true},
		{ "courseId" : "C002" , "studentId" : "S001", "isCompleted" : false},
		{ "courseId" : "C001" , "studentId" : "S003", "isCompleted" : true},
		{ "courseId" : "C003" , "studentId" : "S002", "isCompleted" : false},
		{ "courseId" : "C001" , "studentId" : "S002", "isCompleted" : true},
		{ "courseId" : "C004" , "studentId" : "S003", "isCompleted" : false},
		{ "courseId" : "C002" , "studentId" : "S004", "isCompleted" : true},
		{ "courseId" : "C003" , "studentId" : "S007", "isCompleted" : false},
		{ "courseId" : "C001" , "studentId" : "S005", "isCompleted" : true},
		{ "courseId" : "C004" , "studentId" : "S008", "isCompleted" : false},
		{ "courseId" : "C001" , "studentId" : "S013", "isCompleted" : true}
	]
	)

/*
	Aggregation in MongoDB

		this is the act or process of generating manipulated data and perform operations to create filtered results that helps in analyzing data.

		This helps in creating reports from analyzing the data provided in our documents


	Aggregation pipeline syntax

		db.collections.aggregate(
		[
			{Stage1},
			{Stage2},
			{Stage3}
		]
		)


	Aggregation Pipelines
		Aggregation is done in 2-3 steps typically. The first pipeline is done with the use of $match.

		$match - is used to pass the document that satisfies the given condition 

		syntax - {$math : {"fieldname" : value}}

		$group - is used to group elements or documents together and create an analysis of these grouped documents.

		syntax - {$group : {_id: <id>, fieldResult : "valueResult"}}
*/


	db.course_booking.aggregate(
		[
			{$group : {_id : null, count: {$sum: 1}}}
		]
		);

	db.course_booking.aggregate(
		[
			{$match : {"isCompleted" : true}},
			{$group : {_id : "$courseId" , total : {$sum : 1}}}
		]
		);


	/*
	$sort - 

	$project - this will alow us to show or hide details

		Syntax - {$project : {field : 1 or 0}}
			1 - show and 0 - hide.

	*/

	db.course_booking.aggregate(
		[
			{$match : {"isCompleted" : true}},
			{$project : {"courseId" : 0}}
		]
		);

	db.course_booking.aggregate(
		[
			{$match : {"isCompleted" : true}},
			{$sort : {"courseId" : -1}} // '-1' - descending and '1' - ascending
		]
		)



	db.course_booking.aggregate(
		[
			{$match : {"isCompleted" : true , "studentId" : "S013"}},
			{$group : {_id : null , count : {$sum : 1}}}
		]
		);

	
	db.course_booking.aggregate(
		[
			{$match : {"isCompleted" : true , "studentId" : "S013"}},
			{$count : "courseId"}
		]
		);



	db.orders.insertMany([
        {
                "cust_Id": "A123",
                "amount": 500,
                "status": "A"
        },
        {
                "cust_Id": "A123",
                "amount": 250,
                "status": "A"
        },
        {
                "cust_Id": "B212",
                "amount": 200,
                "status": "A"
        },
        {
                "cust_Id": "B212",
                "amount": 200,
                "status": "D"
        }
])


	db.orders.aggregate(
		[
			{$match : {"status" : "A"}},
			{$group : {_id : "$cust_Id" , total : {$sum : "$amount"}}}
			
		]
		)

	db.orders.aggregate(
		[
			{$match : {"status" : "A"}},
			{$group : {_id : "$cust_Id" , Average result : {$avg : "$amount"}}}
			
		]
		)

	db.orders.aggregate(
		[
			{$group : {_id : "cust_Id" , HighestNumber : {$max : "$amount"}}}
		]
		);


	/*
	db.collections.aggregate([
                   { 
                     $match: {
                          $and: [ 
                              {fieldname: {$in: ["TOYS"]}}, 
                              {type: {$nin: ["BARBIE"]}}, 
                              {time: {$lt:ISODate("2013-12-09T00:00:00Z")}}
                          ]
                     }
                   }
                  ])
	*/

	