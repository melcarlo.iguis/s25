// Activity

// Insert Data

	db.fruits.insertMany([
	{
		"name" : "Banana",
		"supplier" : "Farmer Fruits Inc.",
		"stocks" : 30,
		"price" : 20,
		"onSale" : true
	},
	{
		"name" : "Mango",
		"supplier" : "Mango Magic Inc.",
		"stocks" : 50,
		"price" : 70,
		"onSale" : true
	},
	{
		"name" : "Dragon Fruit",
		"supplier" : "Farmer Fruits Inc.",
		"stocks" : 10,
		"price" : 60,
		"onSale" : true
	},
	{
		"name" : "Grapes",
		"supplier" : "Fruity Co.",
		"stocks" : 30,
		"price" : 100,
		"onSale" : true
	},
	{
		"name" : "Apple",
		"supplier" : "Apple Valley",
		"stocks" : 0,
		"price" : 20,
		"onSale" : false
	},
	{
		"name" : "Papaya",
		"supplier" : "Fruity Co.",
		"stocks" : 15,
		"price" : 60,
		"onSale" : true
	}
	
])

// Solution for number 2

	db.fruits.aggregate(
		[
			{$match : {"onSale" : true}},
			{$count : "totalNumberOfFruitonSale"}
		]
		);


// Solution for number 3
		db.fruits.aggregate(
		[
			{$match : {"stocks" : {$gt : 20}}},
			{$count : "onSaleFruitMoreThan20stock"}
		]
		);
		

// Solution for number 4
		db.fruits.aggregate(
		[
			{$match : {"onSale" : true}},
			{$group : {_id : "$supplier" , avgPriceFruitsOnSale : {$avg : "$price"}}}
		]
		);


// Solution for number 5

		db.fruits.aggregate(
		[
			{$group : {_id : "$supplier" , highestPrice : {$max : "$price"}}}
		]
		);
		

//  Solution for number 6

	db.fruits.aggregate(
		[
			{$group : {_id : "$supplier" , lowestPrice : {$min : "$price"}}}
		]
		);